/*REferencia al paquete express*/

var express = require('express');

/*definimos el objeto para utilizar todo ese paquete*/

var app = express();

/*vamos a realizar un get con el objeto "app" para un HOLA MUNDO*/
//estamos implementando la respuesta a la operaciòn GET,cuando el
//cliente nos llame con esa URL retornamos.
//primer parametro la URL, segundo el callback
app.get('/holamundo',
        function(request,response){
        response.send('Hola Perù');
}); //REQUEST ES POR DONDE LLEGA Y RESPONSE ES
                                     //ES DONDE RESPONDE

//EL puerto por donde va escuchar el servidor node por defecto puerto 3000,
//callback son funciones anonimas
//http://localhost:3003/holamundo
app.listen(3003,function(){
      console.log('Node JS escuchando en el puerto 3000...');

});
